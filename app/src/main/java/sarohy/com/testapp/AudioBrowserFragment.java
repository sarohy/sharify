package sarohy.com.testapp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class AudioBrowserFragment extends Fragment {
    private ArrayList<String> audios;
    private View v;
    private AudioInterface audioInterface;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        audioInterface= (AudioInterface) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.fragment_audio_fragment, container, false);
        ListView gallery = (ListView) v.findViewById(R.id.lv_audio);
        gallery.setAdapter(new ImageAdapter(getActivity()));
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                if (null != audios && !audios.isEmpty()) {
                    File f=new File(audios.get(position));
                    audioInterface.selectedFile(f);
                }

            }
        });
        return v;
    }

    /**
     * The Class ImageAdapter.
     */
    private class ImageAdapter extends BaseAdapter {

        private final LayoutInflater inflater;
        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
            audios = getAllShownImagesPath(context);
            inflater = context.getLayoutInflater();
        }

        public int getCount() {
            return audios.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                //called once

                convertView = inflater.inflate(R.layout.custom_listview_audio_video, parent, false);
                holder = new ViewHolder();
                holder.nameTV = (TextView) convertView.findViewById(R.id.name_file);
                holder.DateTV = (TextView) convertView.findViewById(R.id.name_date);
                holder.imageView=convertView.findViewById(R.id.image);
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.imageView
                        .setLayoutParams(new LinearLayout.LayoutParams(250, 250));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            File f=new File(audios.get(position));
            holder.nameTV.setText(f.getName());
            Date date=new Date(f.lastModified());
            SimpleDateFormat sf=new SimpleDateFormat("dd-mm-yyy hh:mm");
            String s=sf.format(date);
            holder.DateTV.setText(s);
            Glide.with(context).load(audios.get(position))
                    .placeholder(R.drawable.ic_music_video_black_24dp).centerCrop()
                    .into(holder.imageView);
            return convertView;
        }
        class ViewHolder{
            TextView nameTV,DateTV;
            ImageView imageView;
        }
        /**
         * Getting All Images Path.
         *
         * @param activity
         *            the activity
         * @return ArrayList with images Path
         */
        private ArrayList<String> getAllShownImagesPath(Activity activity) {
            Uri uri;
            Cursor cursor;
            int column_index_data, column_index_folder_name;
            ArrayList<String> listOfAllAudios = new ArrayList<String>();
            String absolutePathOfImage = null;
            uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String[] projection = { MediaStore.Audio.AudioColumns.DATA,
                    MediaStore.Audio.Media.DISPLAY_NAME };
            cursor = activity.getContentResolver().query(uri, projection, null,
                    null, null);
            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Audio.AudioColumns.DATA);
            column_index_folder_name = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME);
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data);

                listOfAllAudios.add(absolutePathOfImage);
            }
            return listOfAllAudios;
        }
    }
    public interface AudioInterface{
        public void selectedFile(File f);
    }
}
