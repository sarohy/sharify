package sarohy.com.testapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Environment;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;

public class ReceiveActivity extends AppCompatActivity {

    public final int fileRequestID = 55;
    public final int port = 7950;
    private WifiP2pManager wifiManager;
    private WifiP2pManager.Channel wifichannel;
    private BroadcastReceiver wifiServerReceiver;
    private IntentFilter wifiServerReceiverIntentFilter;
    private String path;
    private File downloadTarget;
    private Intent serverServiceIntent;
    private boolean serverThreadActive;
    TextView serverStatus;
    Button history_btn;
    boolean onrecieveLayout = false;
    private static int PERMISSION_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recieve);
        if (Build.VERSION.SDK_INT >= 23)              //to check wether ot not the OS is marshamallow or above
        {                                             //in 23 and above permissions need to be granted by user/phone using 23 in the emulator
            if (checkPermission())
            {
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {

            // Code for Below 23 API Oriented Device
            // Do next code
        }
        //Block auto opening keyboard
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //setting up
        wifiManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        wifichannel = wifiManager.initialize(this, getMainLooper(), null);
        wifiServerReceiver = new WiFiServerBroadcastReceiver(wifiManager, wifichannel, this);
        wifiServerReceiverIntentFilter = new IntentFilter();;
        wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        wifiServerReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        serverStatus = (TextView) findViewById(R.id.server_status);
        path = Environment.getExternalStorageDirectory().getAbsolutePath();
        downloadTarget = new File(path);
        serverServiceIntent = null;
        serverThreadActive = false;
        registerReceiver(wifiServerReceiver, wifiServerReceiverIntentFilter);
        //If server is already listening on port or transfering data, do not attempt to start server service
        if(!serverThreadActive)
        {
            //Create new thread, open socket, wait for connection, and transfer file
            serverServiceIntent = new Intent(this, ServerService.class);
            serverServiceIntent.putExtra("saveLocation", downloadTarget);
            serverServiceIntent.putExtra("port", new Integer(port));
            serverServiceIntent.putExtra("serverResult", new ResultReceiver(null) {
                @Override
                protected void onReceiveResult(final int resultCode, final Bundle resultData) {

                    if(resultCode == port )
                    {
                        if (resultData == null) {
                            serverThreadActive = false;
                            serverStatus.post(new Runnable() {
                                @Override
                                public void run() {
                                    serverStatus.setText("Server has stopped");
                                }
                            });
                        }
                        else
                        {
                            serverStatus.post(new Runnable() {
                                @Override
                                public void run() {
                                    serverStatus.setText((String)resultData.get("message"));
                                }
                            });
                        }
                    }

                }
            });
            serverThreadActive = true;
            startService(serverServiceIntent);
            //Set status to running
            serverStatus.setText("Waiting for sender to connect");
            wifiManager.discoverPeers(wifichannel,null);
            onrecieveLayout = true;
        }
        else
        {
            //Set status to already running
            wifiManager.discoverPeers(wifichannel,null);
        }
    }
    public void setServerWifiStatus(String message)
    {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    public void setServerStatus(String message)
    {
       Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopServer(null);

        wifiManager.removeGroup(wifichannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(),"Disconected",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int i) {

            }
        });
        //Unregister broadcast receiver
        try {
            unregisterReceiver(wifiServerReceiver);
        } catch (IllegalArgumentException e) {
            // This will happen if the server was never running and the stop
            // button was pressed.
            // Do nothing in this case.
        }
    }

    public void stopServer(View view) {
        //stop download thread
        if(serverServiceIntent != null)
        {
            stopService(serverServiceIntent);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }
}
