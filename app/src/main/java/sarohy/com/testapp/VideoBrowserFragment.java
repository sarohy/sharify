package sarohy.com.testapp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class VideoBrowserFragment extends Fragment {
    private ArrayList<String> videos;
    private View v;
    private VideoInterface videoInterface;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.fragment_video_browser, container, false);
        ListView gallery = (ListView) v.findViewById(R.id.lv_video);
        gallery.setAdapter(new ImageAdapter(getActivity()));
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                if (null != videos && !videos.isEmpty()) {
                    File f=new File(videos.get(position));
                    videoInterface.selectedFile(f);
                }

            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        videoInterface= (VideoInterface) context;
    }
    /**
     * The Class ImageAdapter.
     */
    private class ImageAdapter extends BaseAdapter {

        private final LayoutInflater inflater;
        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
            videos = getAllShownImagesPath(context);
            inflater = context.getLayoutInflater();
        }

        public int getCount() {
            return videos.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.custom_listview_audio_video, parent, false);
                holder = new ViewHolder();
                //called once
                holder.nameTV = (TextView) convertView.findViewById(R.id.name_file);
                holder.DateTV = (TextView) convertView.findViewById(R.id.name_date);
                holder.imageView=convertView.findViewById(R.id.image);
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.imageView
                        .setLayoutParams(new LinearLayout.LayoutParams(250, 250));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            File f=new File(videos.get(position));
            holder.nameTV.setText(f.getName());
            Date date=new Date(f.lastModified());
            SimpleDateFormat sf=new SimpleDateFormat("dd-mm-yyy hh:mm");
            String s=sf.format(date);
            holder.DateTV.setText(s);
            Glide.with(context).load(videos.get(position))
                    .placeholder(R.drawable.ic_ondemand_video_black_24dp).centerCrop()
                    .into(holder.imageView);
            return convertView;
        }
        class ViewHolder{
            TextView nameTV,DateTV;
            ImageView imageView;
        }

        /**
         * Getting All Images Path.
         *
         * @param activity
         *            the activity
         * @return ArrayList with images Path
         */
        private ArrayList<String> getAllShownImagesPath(Activity activity) {
            Uri uri;
            Cursor cursor;
            int column_index_data, column_index_folder_name;
            ArrayList<String> listOfAllVideos = new ArrayList<String>();
            String absolutePathOfImage = null;
            uri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

            String[] projection = { MediaStore.Video.VideoColumns.DATA,
                    MediaStore.Video.Media.DISPLAY_NAME };

            cursor = activity.getContentResolver().query(uri, projection, null,
                    null, null);

            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DATA);
            column_index_folder_name = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME);
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data);

                listOfAllVideos.add(absolutePathOfImage);
            }
            return listOfAllVideos;
        }
    }
    public interface VideoInterface{
        public void selectedFile(File f);
    }
}
