package sarohy.com.testapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void startServer(View view) {
        Intent clientStartIntent = new Intent(this, ReceiveActivity.class);
        startActivity(clientStartIntent);
    }
    public void startClientActivity(View view) {
        Intent clientStartIntent = new Intent(this, BrowseActivity.class);
        startActivity(clientStartIntent);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

