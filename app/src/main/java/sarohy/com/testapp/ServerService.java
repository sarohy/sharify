package sarohy.com.testapp;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.ResultReceiver;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerService extends IntentService {

    private boolean serviceEnabled;
    private int port;
    private File saveLocation;
    private ResultReceiver serverResult;

    public ServerService() {
        super("ServerService");
        serviceEnabled = true;

    }
    @Override
    protected void onHandleIntent(Intent intent) {
        port = ((Integer) intent.getExtras().get("port")).intValue();
        saveLocation = (File) intent.getExtras().get("saveLocation");
        serverResult = (ResultReceiver) intent.getExtras().get("serverResult");
        String fileName = "";
        ServerSocket welcomeSocket = null;
        Socket socket = null;
        try {
            welcomeSocket = new ServerSocket(port);
            while(true && serviceEnabled)
            {
                socket = welcomeSocket.accept();
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os);
                String inputData = "";
                signalActivity("Receiving");
                //Client-Server handshake
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                WifiDirectBundle bundle;
                synchronized (this) {
                    Object o;
                    try {
                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        o = ois.readObject();
                        bundle = (WifiDirectBundle) o;
                        bundle.restoreFile(saveLocation.getAbsolutePath());
                    } finally {

                    }
                }
                baos.close();
                socket.close();
                signalActivity("File Transfer Complete, saved as: " + bundle.getFileName());
            }


        } catch (IOException e) {
            signalActivity(e.getMessage());
        }
        catch(Exception e)
        {
            signalActivity(e.getMessage());
        }
        //Signal that operation is complete
        serverResult.send(port, null);
    }


    public void signalActivity(String message)
    {
        Bundle b = new Bundle();
        b.putString("message", message);
        serverResult.send(port, b);
    }


    public void onDestroy()
    {
        serviceEnabled = false;
        stopSelf();
    }

}
