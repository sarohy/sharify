package sarohy.com.testapp;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BrowseActivity extends AppCompatActivity implements FileBrowserFragment.FileInterface,ImageBrowserFragment.
        ImageInterface,VideoBrowserFragment.VideoInterface,AudioBrowserFragment.AudioInterface {

    private int count=0;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FileBrowserFragment fileBrowser;
    private ImageBrowserFragment imageBrowser;
    private VideoBrowserFragment videoBrowser;
    private AudioBrowserFragment musicBrowser;
    ArrayList<File> files=new ArrayList<>();
    Button btnSelected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        btnSelected= (Button) findViewById(R.id.btn_selected_files);
        btnSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (files.size()>0)
                    showDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.browser_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.send_option:
                Intent intent=new Intent(getApplication(), SendActivity.class);
                intent.putExtra("files",files);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fileBrowser=new FileBrowserFragment();
        imageBrowser=new ImageBrowserFragment();
        videoBrowser =new VideoBrowserFragment();
        musicBrowser =new AudioBrowserFragment();
        adapter.addFragment(imageBrowser,"Image");
        adapter.addFragment(videoBrowser,"Video");
        adapter.addFragment(musicBrowser,"Audio");
        adapter.addFragment(fileBrowser, "File");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void selectedFile(File f)
    {
        boolean isAdded=false;
        for (int i=0;i<files.size();i++)
        {
            if (files.get(i).getAbsolutePath().equals(f.getAbsolutePath())){
                isAdded=true;
                break;
            }
        }
        if (!isAdded) {
            files.add(f);
            count++;
            btnSelected.setText("("+count+") Selected");
        }
        else {
            Toast.makeText(this,"File is already added!!",Toast.LENGTH_SHORT).show();
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void showDialog(){
        final Dialog aDialog;
        final AlertDialog.Builder bDialog = new AlertDialog.Builder(this);
        ListView lv = new ListView(this);
        ViewAdapter books_list = new ViewAdapter(this);
        lv.setAdapter(books_list);
        bDialog.setView(lv);
        bDialog.setCancelable(true);
        bDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        bDialog.setNegativeButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                files.clear();
                count=0;
                btnSelected.setText("("+count+") Selected");
                dialogInterface.dismiss();
            }
        });
        aDialog = bDialog.create();
        aDialog.show();
    }
    private class ViewAdapter extends BaseAdapter {

        private final LayoutInflater inflater;
        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ViewAdapter(Activity localContext) {
            context = localContext;
            inflater = context.getLayoutInflater();
        }

        public int getCount() {
            return files.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                //called once
                convertView = inflater.inflate(R.layout.custom_dialog_listview, parent, false);
                holder = new ViewHolder();
                holder.nameTV = (TextView) convertView.findViewById(R.id.name_file);
                holder.DateTV = (TextView) convertView.findViewById(R.id.name_date);
                holder.imageView=convertView.findViewById(R.id.image);
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.imageView
                        .setLayoutParams(new LinearLayout.LayoutParams(250, 250));
                convertView.setTag(holder);
                holder.deleteBtn=convertView.findViewById(R.id.btn_delete);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            File f=new File(files.get(position).getPath());
            holder.nameTV.setText(f.getName());
            Date date=new Date(f.lastModified());
            SimpleDateFormat sf=new SimpleDateFormat("dd-mm-yyy hh:mm");
            String s=sf.format(date);
            holder.DateTV.setText(s);
            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    files.remove(position);
                    count--;
                    btnSelected.setText("("+count+") Selected");
                    notifyDataSetChanged();
                }
            });
            Glide.with(context).load(files.get(position))
                    .placeholder(R.drawable.ic_insert_drive_file_black_24dp).centerCrop()
                    .into(holder.imageView);
            return convertView;
        }
        class ViewHolder{
            TextView nameTV,DateTV;
            ImageView imageView;
            ImageButton deleteBtn;
        }
    }
}
