package sarohy.com.testapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


public class FileBrowserFragment extends Fragment {
    private String root;
    private String currentPath;
    private ArrayList<String> targets;
    private ArrayList<String> paths;
    private File targetFile;
    View v;
    FileInterface fileInterface;
    public FileBrowserFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fileInterface= (FileInterface) context;
    }
    public static FileBrowserFragment newInstance() {
        FileBrowserFragment fragment = new FileBrowserFragment();;
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       v=inflater.inflate(R.layout.fragment_file_browser, container, false);
        root = "/";
        currentPath = root;
        targets = null;
        paths = null;
        targetFile = null;
        showDir(currentPath);
        return v;
    }
    public void setCurrentPathText(String message)
    {
        TextView fileTransferStatusText = (TextView) v.findViewById(R.id.current_path);
        fileTransferStatusText.setText(message);
    }
    private void showDir(String targetDirectory){
        setCurrentPathText("Current Directory: " + currentPath);
        targets = new ArrayList<String>();
        paths = new ArrayList<String>();
        File f = new File(targetDirectory);
        File[] directoryContents = f.listFiles();
        if (!targetDirectory.equals(root))
        {
            targets.add(root);
            paths.add(root);
            targets.add("../");
            paths.add(f.getParent());
        }
        for(File target: directoryContents)
        {
            paths.add(target.getPath());
            if(target.isDirectory())
            {
                targets.add(target.getName() + "/");
            }
            else
            {
                targets.add(target.getName());
            }
        }
        ListView fileBrowserListView = (ListView) v.findViewById(R.id.file_browser_listview);
        ArrayAdapter<String> directoryData = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, targets);
        fileBrowserListView.setAdapter(directoryData);
        fileBrowserListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View view, int pos,long id) {

                File f = new File(paths.get(pos));
                if(f.isFile())
                {
                    targetFile = f;
                    fileInterface.selectedFile(targetFile);
                }
                else
                {
                    //f must be a dir
                    if(f.canRead())
                    {
                        currentPath = paths.get(pos);
                        showDir(paths.get(pos));
                    }

                }
            }
            // TODO Auto-generated method stub
        });
    }
    public interface FileInterface{
        public void selectedFile(File f);
    }
}
