package sarohy.com.testapp;

import android.net.Uri;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;


public class WifiDirectBundle implements Serializable {
    private String fileName;
    private String mimeType;
    private Long fileSize;
    private byte[] fileContent;

    public WifiDirectBundle() {}

    // adds a file to the bundle, given its URI
    public void setFile(Uri uri) throws IOException {
        File f = new File(uri.getPath());

        fileName = f.getName();
        mimeType = MimeTypeMap.getFileExtensionFromUrl(f.getAbsolutePath());
        fileSize = f.length();

        FileInputStream fin = new FileInputStream(f);
        fileContent = new byte[(int) f.length()];
        fin.read(fileContent);
    }
    // restores the file of the bundle, given its directory (change to whatever
    // fits you better)
    public String restoreFile(String baseDir) {
        File f = new File(baseDir + "/" + fileName);
        try {
            FileOutputStream fos = new FileOutputStream(f);
            if (fileContent != null) {
                fos.write(fileContent);
            }
            fos.close();
            return f.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Long getFileSize() {
        return fileSize;
    }
}
