package sarohy.com.testapp;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class ClientService extends IntentService {
    private boolean serviceEnabled;
    private int port;
    private ResultReceiver clientResult;
    private WifiP2pInfo wifiInfo;
    ArrayList<File> files;
    public ClientService() {
        super("ClientService");
        serviceEnabled = true;
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        port = ((Integer) intent.getExtras().get("port")).intValue();
        files = (ArrayList<File>) intent.getExtras().get("filesToSend");
        clientResult = (ResultReceiver) intent.getExtras().get("clientResult");
        wifiInfo = (WifiP2pInfo) intent.getExtras().get("wifiInfo");
        if(!wifiInfo.isGroupOwner)
        {
            //Getting IP
            InetAddress targetIP = wifiInfo.groupOwnerAddress;
            Socket clientSocket = null;
            OutputStream os = null;
            try {
                for (int i=0;i<files.size();i++) {
                    clientSocket = new Socket(targetIP, port);
                    os = clientSocket.getOutputStream();
                    PrintWriter pw = new PrintWriter(os);
                    InputStream is = clientSocket.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr);
                    //start file transfer
                    WifiDirectBundle bundle = new WifiDirectBundle();
                    bundle.setFile(Uri.fromFile(files.get(i)));
                    ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
                    //writing the object to stream
                    oos.writeObject(bundle);
                    signalActivity("Sent");
                    os.flush();
                    br.close();
                    isr.close();
                    is.close();
                    pw.close();
                    os.close();
                    clientSocket.close();
                }
            } catch (IOException e) {
                signalActivity(e.getMessage());
            }
            catch(Exception e)
            {
                signalActivity(e.getMessage());
            }
        }
        else
        {
            signalActivity("This device is a group owner, therefore the IP address of the " +
                    "target device cannot be determined. Transfer cannot continue");
        }
        clientResult.send(port, null);
    }
    public void signalActivity(String message)
    {
        Bundle b = new Bundle();
        b.putString("message", message);
        clientResult.send(port, b);
    }
    public void onDestroy()
    {
        serviceEnabled = false;
        stopSelf();
    }
}