package sarohy.com.testapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SendActivity extends AppCompatActivity {
    public final int fileRequestID = 98;
    public final int port = 7950;
    private WifiP2pManager wifiManager;
    private WifiP2pManager.Channel wifichannel;
    private BroadcastReceiver wifiClientReceiver;
    private IntentFilter wifiClientReceiverIntentFilter;
    private boolean connectedAndReadyToSendFile;
    private boolean filePathProvided;
    private boolean transferActive;
    Intent clientServiceIntent;
    private WifiP2pDevice targetDevice;
    private WifiP2pInfo wifiInfo;
    ArrayList<File> files;
    ProgressWheel progressWheel;
    String deviceName;
    AlertDialog.Builder dialog ;
    WifiP2pDeviceList peersList;
    AlertDialog pDialog;
    LinearLayout searchingLL;
    LinearLayout sendingLL;
    TextView sendingTV;
    Button sendBtn;
    ListView filesLV;
    Context context;
    ViewAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        dialog=new AlertDialog.Builder(this);
        connectedAndReadyToSendFile = false;
        filePathProvided = false;
        transferActive = false;
        clientServiceIntent = null;
        targetDevice = null;
        wifiInfo = null;
        context=this;
        adapter=new ViewAdapter((Activity) context);
        progressWheel= (ProgressWheel) findViewById(R.id.progress_wheel);
        progressWheel.setVisibility(ProgressWheel.VISIBLE);
        sendingLL= (LinearLayout) findViewById(R.id.ll_sending);
        searchingLL= (LinearLayout) findViewById(R.id.ll_searching);
        sendingLL.setVisibility(LinearLayout.GONE);
        sendBtn= (Button) findViewById(R.id.btn_send);
        filesLV= (ListView) findViewById(R.id.lv_files);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connect();
            }
        });
        sendingTV= (TextView) findViewById(R.id.tv_sendingInfo);
        files = (ArrayList<File>) getIntent().getExtras().get("files");
        for (int i=0;i<files.size();i++) {
            if (files.get(i).isFile()) {
                if (files.get(i).canRead()) {
                    filePathProvided = true;

                } else {
                    filePathProvided = false;
                    setTargetFileStatus("You do not have permission to read the file " + files.get(i).getName());
                }

            } else {
                filePathProvided = false;
                setTargetFileStatus("You may not transfer a directory, please select a single file");
            }
        }
        //setting up
        wifiManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        wifichannel = wifiManager.initialize(this, getMainLooper(), null);
        wifiClientReceiver = new WiFiClientBroadcastReceiver(wifiManager, wifichannel, this);
        wifiClientReceiverIntentFilter = new IntentFilter();;
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        wifiClientReceiverIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        registerReceiver(wifiClientReceiver, wifiClientReceiverIntentFilter);
        setClientFileTransferStatus("Client is currently idle");
        searchForPeers(null);
    }
    public void setTransferStatus(boolean status)
    {
        connectedAndReadyToSendFile = status;
    }

    public void setNetworkToReadyState(boolean status, WifiP2pInfo info, WifiP2pDevice device)
    {
        wifiInfo = info;
        targetDevice = device;
        connectedAndReadyToSendFile = status;
    }

    private void stopClientReceiver()
    {
        try
        {
            unregisterReceiver(wifiClientReceiver);
        }
        catch(IllegalArgumentException e)
        {
            //This will happen if the server was never running and the stop button was pressed.
            //Do nothing in this case.
        }
    }

    public void searchForPeers(View view) {

        progressWheel.setVisibility(ProgressWheel.VISIBLE);
        //Discover peers, no call back method given
        wifiManager.discoverPeers(wifichannel, null);

    }

    public void sendFile() {

        //Only try to send file if there isn't already a transfer active
        if(!transferActive)
        {
            if(!filePathProvided)
            {
                setClientFileTransferStatus("Select a file to send before pressing send");
            }
            else if(!connectedAndReadyToSendFile)
            {
                setClientFileTransferStatus("You must be connected to a server before attempting to send a file");
            }
	        else if(wifiInfo == null)
            {
                setClientFileTransferStatus("Missing Wifi P2P information");
            }
            else
            {
                //Launch client service
                clientServiceIntent= new Intent(this, ClientService.class);
                clientServiceIntent.putExtra("filesToSend", files);
                clientServiceIntent.putExtra("port", new Integer(port));
                clientServiceIntent.putExtra("wifiInfo", wifiInfo);
                clientServiceIntent.putExtra("clientResult", new ResultReceiver(null) {
                    @Override
                    protected void onReceiveResult(int resultCode, final Bundle resultData) {

                        if(resultCode == port )
                        {
                            if (resultData == null) {

                                //Client service has shut down, the transfer may or may not have been successful. Refer to message
                                transferActive = false;
                                Toast.makeText(context,"Sent",Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                            {
                            }
                        }

                    }
                });
                transferActive = true;
                startService(clientServiceIntent);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setClientWifiStatus(String message)
    {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    public void setClientStatus(String message)
    {
        if (message.equals("Connection Status: Connected")){
            searchingLL.setVisibility(LinearLayout.GONE);
            sendingLL.setVisibility(LinearLayout.VISIBLE);
            pDialog.dismiss();
            filesLV.setAdapter(adapter);
            sendingTV.setText("Send to "+ deviceName);
        }
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    public void setClientFileTransferStatus(String message)
    {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    public void setTargetFileStatus(String message)
    {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }
    public void displayPeers(final WifiP2pDeviceList peers)
    {
        peersList=peers;
        //Dialog to show errors/status
        dialog.setTitle("Establishing connection!");
        dialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        //Get list view
        ListView peerView = (ListView) findViewById(R.id.peers_listview);
        //Make array list
        ArrayList<String> peersStringArrayList = new ArrayList<String>();
        //Fill array list with strings of peer names
        for(WifiP2pDevice wd : peers.getDeviceList())
        {
            peersStringArrayList.add(wd.deviceName);
        }

        //Set list view as clickable
        peerView.setClickable(true);

        //Make adapter to connect peer data to list view
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, peersStringArrayList.toArray());

        //Show peer data in listview
        peerView.setAdapter(arrayAdapter);
        peerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View view, int arg2,long arg3) {

                //Get string from textview
                TextView tv = (TextView) view;
                deviceName= tv.getText().toString();

                WifiP2pDevice device = null;

                //Search all known peers for matching name
                for(WifiP2pDevice wd : peers.getDeviceList())
                {
                    if(wd.deviceName.equals(tv.getText()))
                        device = wd;
                }

                if(device != null) {
                    //Connect to selected peer
                    connectToPeer(device);
                    dialog.setMessage("Please accept transfer permission from receiver and wait.");
                    pDialog=dialog.create();
                    pDialog.show();
                }
                else
                {
                    dialog.setMessage("Failed");
                    dialog.show();
                }
            }
            // TODO Auto-generated method stub
        });
        progressWheel.setVisibility(ProgressWheel.GONE);
    }

    public void connectToPeer(final WifiP2pDevice wifiPeer)
    {
        this.targetDevice = wifiPeer;
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = wifiPeer.deviceAddress;
        config.groupOwnerIntent = 0;
        wifiManager.connect(wifichannel, config, new WifiP2pManager.ActionListener()  {
            public void onSuccess() {
                connectedAndReadyToSendFile=true;
            }

            public void onFailure(int reason) {
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopServer(null);

        wifiManager.removeGroup(wifichannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(),"Disconnected",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int i) {

            }
        });
        //Unregister broadcast receiver
        try {
            unregisterReceiver(wifiClientReceiver);
        } catch (IllegalArgumentException e) {
            // This will happen if the server was never running and the stop
            // button was pressed.
            // Do nothing in this case.
        }
    }
    public void stopServer(View view) {
        if (clientServiceIntent != null) {
                stopService(clientServiceIntent);
        }
    }
    void connect()
    {
        WifiP2pDevice device = null;
        //Search all known peers for matching name
        for(WifiP2pDevice wd : peersList.getDeviceList())
        {
            if(wd.deviceName.equals(deviceName))
                device = wd;
        }
        if(device != null) {
            //Connect to selected peer
            connectToPeer(device);
            sendFile();
        }
        else
        {
            dialog.setMessage("Failed");
            dialog.show();
        }
        sendingTV.setText("Sending......");
    }

    private class ViewAdapter extends BaseAdapter{

        private final LayoutInflater inflater;
        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ViewAdapter(Activity localContext) {
            context = localContext;
            inflater = context.getLayoutInflater();
        }

        public int getCount() {
            return files.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.custom_listview_audio_video, parent, false);

                holder = new ViewHolder();
                //called once
                holder.nameTV = (TextView) convertView.findViewById(R.id.name_file);
                holder.DateTV = (TextView) convertView.findViewById(R.id.name_date);
                holder.imageView=convertView.findViewById(R.id.image);
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.imageView
                        .setLayoutParams(new LinearLayout.LayoutParams(250, 250));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            File f=new File(files.get(position).getPath());
            holder.nameTV.setText(f.getName());
            Date date=new Date(f.lastModified());
            SimpleDateFormat sf=new SimpleDateFormat("dd-mm-yyy hh:mm");
            String s=sf.format(date);
            holder.DateTV.setText(s);
            Glide.with(context).load(files.get(position))
                    .placeholder(R.drawable.ic_insert_drive_file_black_24dp).centerCrop()
                    .into(holder.imageView);
            return convertView;
        }
        class ViewHolder{
            TextView nameTV,DateTV;
            ImageView imageView;
        }
    }
}
